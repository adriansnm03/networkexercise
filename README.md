# NetworkExercise

This network excercise goal is to determine if two elements are connected in a network, either directly or through a
series of connections. We do not care about the path. but for this problem the fact that there are two paths is irrelevant.

## Run the network exercise
Network exercise is running from src/Main.java file.

## Tests
Network exercise has unit tests under src/tests folder.
