import com.network.Network;
import com.network.Node;

public class Main {
    public static void main(String[] args) {
        Network network = new Network(8);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));
        network.addNode(new Node(4));
        network.addNode(new Node(5));
        network.addNode(new Node(6));
        network.addNode(new Node(7));
        network.addNode(new Node(8));

        network.connect(1, 2);
        network.connect(2, 6);
        network.connect(1, 6);
        network.connect(2, 4);
        network.connect(5, 8);

        network.query(1, 6); // CONNECTED
        network.query(1, 2); // CONNECTED
        network.query(5, 8); // CONNECTED
        network.query(5, 6); // NOT CONNECTED
        network.query(1, 4); // CONNECTED
        network.query(3, 7); // NOT CONNECTED
        network.query(6, 8); // NOT CONNECTED

    }
}
