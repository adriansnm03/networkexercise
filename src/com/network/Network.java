package com.network;

import com.network.exceptions.NetworkException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Network class that contains its nodes and number of nodes.
 *
 * @author Adrian Becerra
 */
public class Network {
    List<Node> nodes = new ArrayList<>();
    int numberOfNodes = 0;

    public Network(int numberOfNodes) {
        this.numberOfNodes = numberOfNodes;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    public void setNumberOfNodes(int numberOfNodes) {
        this.numberOfNodes = numberOfNodes;
    }

    /**
     * Add a node to the network
     *
     * @param node
     */
    public void addNode(Node node) {
        if (nodes.size() < numberOfNodes) {
            nodes.add(node);
            System.out.println("New node with id " + node.getNodeId() + " added to the network");
        } else {
            throw new NetworkException("It was not able to create the node " + node.getNodeId() + ". " +
                    "Maximum nodes quantity reached. com.network.Network size: " + numberOfNodes);
        }
    }

    /**
     * Connect two given nodes.
     *
     * @param nodeId1
     * @param nodeId2
     */
    public void connect(int nodeId1, int nodeId2) {
        if (nodeId1 == nodeId2) {
            throw new NetworkException("You can't connect two nodes with the same id");
        }

        Node node1 = getNode(nodeId1);
        Node node2 = getNode(nodeId2);

        if (node1 != null && node2 != null) {
            node1.getNodesConnected().add(node2);
            node2.getNodesConnected().add(node1);
        }

    }

    /**
     * Returns a node given an id
     *
     * @param nodeId
     * @return com.network.Node
     */
    private Node getNode(int nodeId) {
        Node node = null;

        if (nodes.stream().filter(n -> n.getNodeId() == nodeId).findFirst().isPresent()) {
            Optional<Node> onode = nodes.stream().filter(n -> n.getNodeId() == nodeId).findFirst();
            node = onode.get();
        } else {
            throw new NetworkException("The node " + nodeId + " does not exists in the network");
        }
        return node;
    }

    /**
     * Query if 2 given nodes ids exists
     *
     * @param nodeId1
     * @param nodeId2
     * @return boolean
     */
    public boolean query(int nodeId1, int nodeId2) {
        Node node1 = validateNodeExists(nodeId1);
        Node node2 = validateNodeExists(nodeId2);

        boolean result = areConnected(node1, node2);

        if (!result) {
            System.out.println("com.network.Node " + node1.getNodeId() + " and node " + node2.getNodeId() + " are not connected");
        } else {
            System.out.println("com.network.Node " + node1.getNodeId() + " and node " + node2.getNodeId() + " are CONNECTED!");
        }

        // Reset visited flag
        nodes.stream().forEach(n -> n.setVisited(false));

        return result;
    }

    /**
     * Returns if a node is connected with other
     *
     * @param node
     * @param nodeToSearch
     * @return boolean
     */
    private boolean areConnected(Node node, Node nodeToSearch) {
        node.setVisited(true);
        if (!node.getNodesConnected().contains(nodeToSearch)) {
            for (Node n : node.getNodesConnected()) {
                if (n.isVisited()) {
                    continue;
                }
                if (areConnected(n, nodeToSearch)) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Validate if the node exists already in the network
     *
     * @param nodeId
     * @return
     */
    private Node validateNodeExists(int nodeId) {
        Optional<Node> opNode;
        try {
            opNode = nodes.stream().filter(n -> n.getNodeId() == nodeId).findFirst();
        } catch (NoSuchElementException e) {
            throw new NetworkException("The node" + nodeId + " does not exists in the com.network.Network");
        }
        if (opNode.isPresent()) {
            return opNode.get();
        } else {
            throw new NetworkException("The node" + nodeId + "doesn't exists in the com.network.Network");
        }
    }
}
