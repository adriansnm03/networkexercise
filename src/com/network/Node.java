package com.network;

import java.util.HashSet;
import java.util.Set;

/**
 * Node class represent a node in the network.
 */
public class Node {
    private int nodeId;
    private Set<Node> nodesConnected = new HashSet<>();
    private boolean isVisited = false;

    public Node(int nodeId, Set<Node> nodesConnected) {
        this.nodeId = nodeId;
        this.nodesConnected = nodesConnected;
    }

    public Node(int nodeId) {
        this.nodeId = nodeId;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public Set<Node> getNodesConnected() {
        return nodesConnected;
    }

    public void setNodesConnected(Set<Node> nodesConnected) {
        this.nodesConnected = nodesConnected;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }
}
