package com.network.exceptions;

public class NetworkException extends RuntimeException {
    public NetworkException(String message) {
        super(message);
    }
}
