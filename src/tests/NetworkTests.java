package tests;

import com.network.Network;
import com.network.exceptions.NetworkException;
import com.network.Node;
import org.junit.Assert;
import org.junit.Test;


public class NetworkTests {
    @Test(expected = NetworkException.class)
    public void connectNetworkNodesMaximumReachExceptionTest() {
        Network network = new Network(8);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));
        network.addNode(new Node(4));
        network.addNode(new Node(5));
        network.addNode(new Node(6));
        network.addNode(new Node(7));
        network.addNode(new Node(8));
        network.addNode(new Node(9));
    }

    @Test(expected = NetworkException.class)
    public void connectSameNodeExceptionTest() {
        Network network = new Network(8);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));
        network.addNode(new Node(4));
        network.addNode(new Node(5));
        network.addNode(new Node(6));
        network.addNode(new Node(7));
        network.addNode(new Node(8));

        network.connect(1, 1);
    }

    @Test
    public void querySameNodesTest() {
        Network network = new Network(3);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));

        network.connect(1, 2);
        network.connect(1, 2);

        Assert.assertTrue(network.query(1, 2)); // Connected
        Assert.assertTrue(network.query(1, 2)); // Connected

    }

    @Test(expected = NetworkException.class)
    public void queryNodeDoesNotExistsTest() {
        Network network = new Network(3);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));

        network.connect(1, 3);

        Assert.assertTrue(network.query(1, 7));
    }

    @Test
    public void queryNodeConnectedTwolevelsTest() {
        Network network = new Network(8);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));
        network.addNode(new Node(4));

        network.connect(1, 2);
        network.connect(2, 3);
        network.connect(2, 4);

        Assert.assertTrue(network.query(1, 4));
    }

    @Test
    public void queryNodeNotConnectedTest() {
        Network network = new Network(4);
        network.addNode(new Node(1));
        network.addNode(new Node(2));
        network.addNode(new Node(3));
        network.addNode(new Node(6));

        network.connect(1, 2);
        network.connect(2, 6);

        Assert.assertFalse(network.query(1, 3)); // Connected
    }
}

